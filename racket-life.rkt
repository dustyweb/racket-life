#lang racket

;;; racket-life --- Simple Game of Life demo in Racket
;;; Copyright © 2017 Christopher Lemmer Webber <cwebber@dustycloud.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require pict racket/draw
         images/icons/control images/icons/style
         2htdp/universe
         racket/generic)

(struct gridpos
  (x y)
  #:transparent)

(define-generics gameworld
  (gameworld-on-mouse gameworld x y mouse-event)
  (gameworld-on-key gameworld key-event)
  (gameworld-on-tick gameworld)
  (gameworld-render gameworld)
  #:fallbacks
  [(define (gameworld-on-mouse gw x y mouse-event)
     gw)
   (define (gameworld-on-key gw key-event)
     gw)
   (define (gameworld-on-tick gw)
     gw)])

(struct editworld
  (grid                ; grid currently being edited
   click-gridpos       ; position currently clicked down, or #f
   hover-gridpos)      ; where the mouse is currently hovering
  #:methods gen:gameworld
  [(define (gameworld-on-mouse gw x y mouse-event)
     (define current-gridpos
       (find-gridpos x y tile-size))
     (define (add-hover gw)
       (struct-copy
        editworld gw
        [hover-gridpos current-gridpos]))
     (add-hover
      (match mouse-event
        ("button-down"
         (struct-copy
          editworld gw
          [click-gridpos current-gridpos]))
        ("button-up"
         ;; either cancel or activate press
         (let ([new-gridpos current-gridpos])
           (if (and (editworld-click-gridpos gw)
                    (equal? (editworld-click-gridpos gw)
                            new-gridpos))
               (struct-copy
                editworld gw
                [grid (grid-flip-tile (editworld-grid gw) new-gridpos)]
                [click-gridpos #f])
               (struct-copy
                editworld gw
                [click-gridpos #f]))))
        ("leave"
         (struct-copy
          editworld gw
          [click-gridpos #f]))
        (_ gw))))
   (define (gameworld-on-key gw key-event)
     (cond
      ((key=? key-event " ")
       (runworld (editworld-grid gw)
                 frames-per-update
                 0 (editworld-grid gw)))
      ((key=? key-event "q")
       #f)
      (else gw)))
   (define (gameworld-render gw)
     (let* ([click-gridpos (editworld-click-gridpos gw)]
            [hover-matches-click
             (and click-gridpos
                  (equal? click-gridpos
                          (editworld-hover-gridpos gw)))])
       (let ([result
              (render-board (editworld-grid gw)
                            (and hover-matches-click click-gridpos)
                            #:bitmap (*display-bitmap*))])
         result)))])

(struct runworld
  (running-grid        ; current grid
   countdown           ; ticks until next grid
   generation          ; count of current generations
   edit-grid)          ; edit grid before running
  #:methods gen:gameworld
  [(define (gameworld-on-tick gw)
     (if (= (runworld-countdown gw) 0)
         (struct-copy
          runworld gw
          [running-grid (next-generation (runworld-running-grid gw))]
          [countdown frames-per-update]
          [generation (+ (runworld-generation gw) 1)])
         (struct-copy
          runworld gw
          [countdown (- (runworld-countdown gw) 1)])))
   (define (gameworld-on-key gw key-event)
     (cond
      ((key=? key-event " ")
       (editworld
        (runworld-edit-grid gw)
        #f #f))
      ((key=? key-event "q")
       #f)
      (else gw)))
   (define (gameworld-render gw)
     (render-board (runworld-running-grid gw)
                   #:bitmap (*display-bitmap*)))])

(define tile-size 20)
(define frames-per-update 7)

(define (mk-square color z-ratio [material glass-icon-material])
  (bitmap-render-icon 
   (pict->bitmap (filled-rectangle tile-size tile-size
                                   #:color color))
   z-ratio
   material))

(define off-square (mk-square "gray" 1/3))
(define off-square-pressed (mk-square "gray" 2/3))
(define on-square (mk-square "black" 2/3))
(define on-square-pressed
  (pict->bitmap
   (cc-superimpose
    (filled-rectangle tile-size tile-size
                      #:color "black")
    (cellophane (bitmap off-square-pressed) 0.4))))

;; @@: We could make this a lot faster if we didn't keep regenerating
;; make-platform-bitmap and reused the bitmap/bitmap-dc

(define (render-board grid [pressed #f]
                      #:bitmap [bmp #f])
  (define grid-y-size
    (or (*grid-y*)
        (vector-length grid)))
  (define grid-x-size
    (or (*grid-x*)
        (vector-length (vector-ref grid 0))))
  (let* ([bmp
          (or bmp
              (make-platform-bitmap (* grid-x-size tile-size)
                                    (* grid-y-size tile-size)))]
         [bdc
          (make-object bitmap-dc% bmp)])
    (let lp-rows ([y 0])
      (unless (= y grid-y-size)
        (let ([row (vector-ref grid y)])
          (let lp-cols ([x 0])
            (unless (= x grid-x-size)
              (let ([col (vector-ref row x)]
                    [i-am-pressed (and pressed
                                       (= (gridpos-x pressed) x)
                                       (= (gridpos-y pressed) y))])
                (send bdc draw-bitmap
                      (if col
                          (if i-am-pressed
                              on-square-pressed
                              on-square)
                          (if i-am-pressed
                              off-square-pressed
                              off-square))
                      (* tile-size x) (* tile-size y)
                      'opaque))
              (lp-cols (+ x 1)))))
        (lp-rows (+ y 1))))
    bmp))

(define (empty-grid x y)
  (make-vector x (make-vector y #f)))

(define (grid-flip-tile grid pos)
  (define new-grid
    (vector-copy grid))
  (define new-row
    (vector-copy (vector-ref new-grid (gridpos-y pos))))
  (vector-set! new-row (gridpos-x pos)
               (not (vector-ref new-row (gridpos-x pos))))
  (vector-set! new-grid (gridpos-y pos) new-row)
  new-grid)

(define (find-gridpos x y block-size)
  (gridpos (quotient x block-size)
           (quotient y block-size)))

(define (find-alive-neighbors grid gridpos)
  (define grid-y-size
    (or (*grid-y*)
        (vector-length grid)))
  (define grid-x-size
    (or (*grid-x*)
        (vector-length (vector-ref grid 0))))
  (define (liveness-at x y)
    (cond
     ((or (< x 0) (< y 0)
          (>= x grid-x-size) (>= y grid-y-size))
      0)
     ((vector-ref (vector-ref grid y) x)
      1)
     (else 0)))
  (define x (gridpos-x gridpos))
  (define y (gridpos-y gridpos))
  (+ (liveness-at (- x 1) (- y 1))
     (liveness-at x (- y 1))
     (liveness-at (+ x 1) (- y 1))
     (liveness-at (- x 1) y)
     (liveness-at (+ x 1) y)
     (liveness-at (- x 1) (+ y 1))
     (liveness-at x (+ y 1))
     (liveness-at (+ x 1) (+ y 1))))

(define (next-generation grid)
  (define grid-y-size
    (or (*grid-y*)
        (vector-length grid)))
  (define grid-x-size
    (or (*grid-x*)
        (vector-length (vector-ref grid 0))))
  (define new-grid
    (make-vector grid-y-size #f))
  (define (new-liveness gridpos)
    (define alive-neighbors
      (find-alive-neighbors grid gridpos))
    (define i-am-alive
      (vector-ref (vector-ref grid (gridpos-y gridpos))
                  (gridpos-x gridpos)))
    (cond
     ;; lives on!
     ((and i-am-alive (or (= alive-neighbors 2)
                          (= alive-neighbors 3)))
      #t)
     ;; death by over/underpopulation
     (i-am-alive
      #f)
     ;; reproduction by nearby live cells
     ((= alive-neighbors 3)
      #t)
     ;; dead cells remain dead
     (else #f)))

  (for ([y (in-range grid-y-size)])
    (define new-row
      (make-vector grid-x-size #f))
    (vector-set! new-grid y new-row)
    (for ([x (in-range grid-x-size)])
      (vector-set! new-row x
                   (new-liveness (gridpos x y)))))
  new-grid)

;; We do this stupid thing because allocating new bitmaps
;; is super expensive.  This is slightly faster, keeping around
;; the old bitmap, but if big-bang sees the same image come through,
;; it assumes we're being functional and doesn't render it again
(define *display-bitmap*
  (make-parameter #f))
(define *dumble-buffer*
  (make-parameter #f))
(define *grid-x*
  (make-parameter #f))
(define *grid-y*
  (make-parameter #f))


(define (start [initial-grid (empty-grid 20 20)])
  ;; TODO: We should parameterize the rows/cols
  (define grid-y-size
    (vector-length initial-grid))
  (define grid-x-size
    (vector-length (vector-ref initial-grid 0)))
  (parameterize* ([*display-bitmap*
                   (make-platform-bitmap (* grid-x-size tile-size)
                                         (* grid-y-size tile-size))]
                  [*dumble-buffer*
                   (make-platform-bitmap (* grid-x-size tile-size)
                                         (* grid-y-size tile-size))]
                  [*grid-x* grid-x-size]
                  [*grid-y* grid-y-size])
    (big-bang (editworld initial-grid #f #f)
              [to-draw
               (lambda (gw)
                 (let ([buf1 (*display-bitmap*)]
                       [buf2 (*dumble-buffer*)])
                   (*display-bitmap* buf2)
                   (*dumble-buffer* buf1))
                 (gameworld-render gw))]
              [on-mouse gameworld-on-mouse]
              [on-key gameworld-on-key]
              [on-tick gameworld-on-tick]
              [stop-when
               (lambda (gw)
                 (not gw))]
              [close-on-stop #t])))

(define *test-grid*
  '#(#(#f #f #f #f #f #f #f)
     #(#f #f #f #f #f #f #f)
     #(#f #f #f #t #f #f #f)
     #(#f #f #f #f #t #f #f)
     #(#f #f #t #t #t #f #f)
     #(#f #f #f #f #f #f #f)
     #(#f #f #f #f #f #f #f)))

(provide start empty-grid)
